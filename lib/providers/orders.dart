import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:shop_app/providers/cart.dart';

class OrderItem {
  final String id;
  final double amount;
  final List<CartItem> products;
  final DateTime dateTime;

  OrderItem({
    @required this.id,
    @required this.amount,
    @required this.products,
    @required this.dateTime,
  });
}

class Orders with ChangeNotifier {
  final String _token;
  final String _userId;
  List<OrderItem> _orders;

  Orders(this._token,this._userId, this._orders);

  List<OrderItem> get orders {
    return [..._orders];
  }

  void remove(String id) {
    _orders.removeWhere((order) => order.id == id);
    notifyListeners();
  }

  Future<void> fetchOrders() async {
    final url =
        'https://flutter-cool-shop-default-rtdb.firebaseio.com/orders/$_userId.json?auth=$_token';
    final res = await http.get(url);
    //print(json.decode(res.body));
    final List<OrderItem> fetchedOrders = [];
    final resDataMap = json.decode(res.body) as Map<String, dynamic>;
    if (resDataMap == null) {
      return;
    }
    resDataMap.forEach((key, val) {
      fetchedOrders.add(OrderItem(
        id: key,
        amount: val['amount'],
        dateTime: DateTime.parse(val['dateTime']),
        products: (val['products'] as List<dynamic>)
            .map((item) => CartItem(
                  id: item['id'],
                  price: item['price'],
                  quantity: item['quantity'],
                  title: item['title'],
                ))
            .toList(),
      ));
    });
    _orders = fetchedOrders.reversed.toList();
    notifyListeners();
  }

  Future<void> addOrder(List<CartItem> cart, double total) async {
    final url =
        'https://flutter-cool-shop-default-rtdb.firebaseio.com/orders/$_userId.json?auth=$_token';
    final timestamp = DateTime.now();
    final res = await http.post(
      url,
      body: json.encode({
        'amount': total,
        'dateTime': DateTime.now().toIso8601String(),
        'products': cart
            .map((cp) => {
                  'id': cp.id,
                  'title': cp.title,
                  'quantity': cp.quantity,
                  'price': cp.price,
                })
            .toList()
      }),
    );

    _orders.insert(
      //add orders at the beginning of the list
      0,
      OrderItem(
        id: json.decode(res.body)['name'],
        amount: total,
        dateTime: timestamp,
        products: cart,
      ),
    );
    notifyListeners();
  }
}
