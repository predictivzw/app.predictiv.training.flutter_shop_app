import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'product.dart';
import '../models/http_exception.dart';
//import '../default_products.dart';

class Products with ChangeNotifier {
  final String _token;
  final String _userId;
  List<Product> _items;

  Products(this._token, this._userId, this._items);

  List<Product> get items {
    return [..._items];
  }

  List<Product> get favoriteItems {
    return _items.where((prod) => prod.isFavorite).toList();
  }

  Product findByID(String id) {
    return items.firstWhere((prod) => prod.id == id);
  }

  Future<void> fetchProducts([bool filterByUser = false]) async {
    final filterString =
        filterByUser ? '&orderBy="creatorId"&equalTo="$_userId"' : '';
    final url =
        'https://flutter-cool-shop-default-rtdb.firebaseio.com/products.json?auth=$_token$filterString';

    try {
      final res = await http.get(url);
      final resDataMap = json.decode(res.body) as Map<String, dynamic>;
      final List<Product> fetchedProducts = [];
      if (resDataMap == null) {
        return;
      }

      final favUrl =
          'https://flutter-cool-shop-default-rtdb.firebaseio.com/userFavorites/$_userId.json?auth=$_token';
      final favRes = await http.get(favUrl);
      final favResDataMap = json.decode(favRes.body);

      resDataMap.forEach((key, value) {
        fetchedProducts.add(Product(
          id: key,
          title: value['title'],
          description: value['description'],
          imageUrl: value['imageUrl'],
          price: value['price'],
          isFavorite:
              favResDataMap == null ? false : favResDataMap[key] ?? false,
        ));
      });
      _items = fetchedProducts;
      notifyListeners();
      print(json.decode(res.body));
    } catch (err) {
      throw err;
    }
  }

  Future<void> addProduct(Product product) async {
    final url =
        'https://flutter-cool-shop-default-rtdb.firebaseio.com/products.json?auth=$_token';

    try {
      final res = await http.post(
        url,
        body: json.encode({
          'title': product.title,
          'description': product.description,
          'price': product.price,
          'imageUrl': product.imageUrl,
          'creatorId': _userId,
        }),
      );

      final prod = Product(
        id: json.decode(res.body)['name'],
        title: product.title,
        isFavorite: product.isFavorite,
        price: product.price,
        description: product.description,
        imageUrl: product.imageUrl,
      );

      _items.add(prod);
      notifyListeners();
    } catch (err) {
      throw err;
    }
  }

  Future<void> updateProduct(Product update) async {
    final prodIndex = _items.indexWhere((prod) => prod.id == update.id);
    if (prodIndex >= 0) {
      final url =
          'https://flutter-cool-shop-default-rtdb.firebaseio.com/products/${update.id}.json?auth=$_token';
      try {
        await http.put(url,
            body: json.encode({
              'title': update.title,
              'description': update.description,
              'price': update.price,
              'imageUrl': update.imageUrl,
              'isFavorite': update.isFavorite
            }));
        _items[prodIndex] = update;
        notifyListeners();
      } catch (err) {
        print(err);
        throw err;
      }
    } else {
      print('...failed to update product!!!');
    }
  }

  Future<void> deleteProduct(String id) async {
    final url =
        'https://flutter-cool-shop-default-rtdb.firebaseio.com/products/$id.json?auth=$_token';
    var toDelete = _items.firstWhere((prod) => prod.id == id);
    _items.removeWhere((prod) => prod.id == id);
    notifyListeners();
    try {
      final res = await http.delete(url);

      if (res.statusCode >= 400) {
        throw HttpException('Could not delete product!');
      }

      toDelete = null;
    } catch (err) {
      print(err);
      _items.add(toDelete);
      notifyListeners();
      throw err;
    }
  }
}
