import 'package:flutter/material.dart';

class CartItem {
  final String id;
  final String title;
  final int quantity;
  final double price;

  CartItem({
    @required this.id,
    @required this.title,
    @required this.quantity,
    @required this.price,
  });
}

class Cart with ChangeNotifier {
  Map<String, CartItem> _items = {};

  Map<String, CartItem> get items {
    return {..._items};
  }

  int get productCount {
    return _items.length;
  }

  int get itemCount {
    return _items.values.fold(0, (val, item) {
      return val + item.quantity;
    });
  }

  double get totalAmount {
    return _items.values.fold(0.0, (val, item) {
      return val + (item.quantity * item.price);
    });
  }

  void addItem({
    @required String id,
    @required String price,
    @required String title,
  }) {
    if (_items.containsKey(id)) {
      _items.update(
          id,
          (curr) => CartItem(
                id: curr.id,
                quantity: curr.quantity + 1,
                title: curr.title,
                price: curr.price,
              ));
    } else {
      _items.putIfAbsent(
        id,
        () => CartItem(
          id: DateTime.now().toString(),
          title: title,
          price: double.parse(price),
          quantity: 1,
        ),
      );
    }
    notifyListeners();
  }

  void removeProduct(String id) {
    _items.remove(id);
    notifyListeners();
  }

  void removeProductItem(String id) {
    if (!_items.containsKey(id)) {
      return;
    }

    if (_items[id].quantity > 1) {
      _items.update(
        id,
        (curr) => CartItem(
          id: curr.id,
          price: curr.price,
          quantity: curr.quantity - 1,
          title: curr.title,
        ),
      );
    } else {
      _items.remove(id);
    }

    notifyListeners();
  }

  void increaseProductItemCount(String id) {
    if (!_items.containsKey(id)) {
      return;
    }

    if (_items[id].quantity >= 1) {
      _items.update(
        id,
        (curr) => CartItem(
          id: curr.id,
          price: curr.price,
          quantity: curr.quantity + 1,
          title: curr.title,
        ),
      );
    }
    notifyListeners();
  }

  void clearCart() {
    _items = {};
    notifyListeners();
  }
}
